#[macro_use] extern crate lazy_static;

mod instructions;
mod parser;
mod lexer;
mod util;
mod encoding;

use std::fs::File;
use std::fs;
use std::io::Read;
use std::io::Result as ioResult;

use parser::Parser;
use instructions::build_binary;

const NINTENDO_LOGO_DATA_START : usize = 0x0104;
const NINTENDO_LOGO_DATA_END   : usize = 0x0134;
const NINTENDO_LOGO_DATA_LENGTH: usize = NINTENDO_LOGO_DATA_END - NINTENDO_LOGO_DATA_START;
const NINTENDO_LOGO_DATA: [u8; 0x30] = [
    0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B, 0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00,
    0x0D, 0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E, 0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD,
    0xD9, 0x99, 0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC, 0xDD, 0xDC, 0x99, 0x9F, 0xBB,
    0xB9, 0x33, 0x3E
];

const HEADER_START : usize = 0x134;
const HEADER_END   : usize = 0x14D;

const GLOBAL_CHECKSUM_START: usize = 0x014E;
const GLOBAL_CHECKSUM_END  : usize = 0x0150;

fn main() {
    let mut input_file_path: String  = "".into();
    let mut output_file_path: String = "".into();
    {
        let mut parser = argparse::ArgumentParser::new();

        parser.refer(&mut input_file_path)
            .add_argument("input_file", argparse::Store,
                          "Input assembly file (asm)")
            .required();
        
        parser.refer(&mut output_file_path)
            .add_argument("output_file", argparse::Store,
                          "Output binary file")
            .required();

        parser.parse_args_or_exit();
    }
    let input_file_path = input_file_path;
    
    let file_contents = read_file(&input_file_path);
    let program = Parser::new(&file_contents, &input_file_path).parse();
    let mut binary = Box::leak(build_binary(program)
        .expect("Error building binary"));
    write_nintendo_logo_data(&mut binary);
    header_checksum(&mut binary);
    global_checksum(&mut binary);
    write_binary(&output_file_path, &binary)
        .expect("Error writing binary");
}

fn read_file(input_path: &str) -> String {
    let mut file = match File::open(input_path) {
        Ok(f) => f,
        Err(err) => panic!("Can't open {}: {}", input_path, err)
    };

    let mut contents = String::new();
    match file.read_to_string(&mut contents) {
        Ok(contents) => contents,
        Err(_) => panic!("Can't read {}", input_path)
    };

    contents
}

fn write_nintendo_logo_data(binary: &mut [u8]) {
    for i in 0..NINTENDO_LOGO_DATA_LENGTH {
        let add = NINTENDO_LOGO_DATA_START + i;
        binary[add] = NINTENDO_LOGO_DATA[i];
    }
}

fn header_checksum(binary: &mut [u8]) {
    let mut sum = 0u8;
    for addr in HEADER_START..HEADER_END {
        sum = sum.wrapping_add(binary[addr]);
    }
    binary[HEADER_END] = 0i8.wrapping_sub(sum as i8).wrapping_sub(0x19u8 as i8) as u8;
}

fn write_binary(output_path: &str, data: &[u8]) -> ioResult<()> {
    fs::write(output_path, data)
}

fn global_checksum(binary: &mut [u8]) {
    let mut sum = 0u16;
    for addr in (0..GLOBAL_CHECKSUM_START)
        .chain(GLOBAL_CHECKSUM_END..binary.len()) {
        sum = sum.wrapping_add(binary[addr] as u16);
    }
    binary[GLOBAL_CHECKSUM_START]   = (sum >> 8) as u8;
    binary[GLOBAL_CHECKSUM_START+1] = (sum & 0x00FF) as u8;
}