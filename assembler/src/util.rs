use crate::lexer::SourceLocation;

pub fn fatal(msg: String, source: &SourceLocation) -> ! {
    panic!("Error in {}: {}", source, msg);
}

pub fn fatal_generic(msg: String) -> ! {
    panic!("Error: {}", msg);
}