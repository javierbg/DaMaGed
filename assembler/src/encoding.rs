use std::collections::HashMap;
use crate::parser::{Op, Argument};
use crate::parser::Argument::*;
use crate::parser::Indirect::*;

type Decoded = Result<Vec<u8>, String>;

pub fn encode_op(op: Op, address: u16, label_addresses: &HashMap<String, u16>) -> Decoded {
    let mnemonic = op.mnemonic;
    
    match mnemonic.as_str() {
        "JP"  => encode_jp_op(op.params, label_addresses),
        "LD"  => encode_ld_op(op.params),
        "ADD" => encode_add_op(op.params),
        "ADC" | "SBC" => encode_adc_sbc_op(&mnemonic, op.params),
        "SUB" | "AND" | "XOR" | "OR" | "CP" =>
            encode_sub_logical_op(&mnemonic, op.params),
        "INC" => encode_inc_op(op.params),
        "DEC" => encode_dec_op(op.params),
        "LDH" => encode_ldh_op(op.params),
        "PUSH" => encode_push_op(op.params),
        "POP" => encode_pop_op(op.params),
        "JR" => encode_jr_op(op.params, address, &label_addresses),
        "LDD" => encode_ldd_op(op.params),
        "LDI" => encode_ldi_op(op.params),
        "LDHL" => encode_ldhl_op(op.params),
        "RET" => encode_ret_op(op.params),
        "CALL" => encode_call_op(op.params, &label_addresses),
        "RST" => encode_rst_op(op.params),
        "RLC" | "RRC" | "RL" | "RR" | "SLA" |
        "SRA" | "SWAP" | "SRL" => encode_cbprefix_rotate_op(&mnemonic, op.params),
        "BIT" | "RES" | "SET" => encode_cbprefix_bit_op(&mnemonic, op.params),
        m if op.params.is_empty() => encode_no_argument_op(m),
        _ => Err(format!("Unrecognized mnemonic {}", mnemonic))
    }
}

fn encode_cbprefix_bit_op(mnemonic: &str, params: Vec<Argument>) -> Decoded {
    let base_code: u8 = match mnemonic {
        "BIT" => 0x40,
        "RES" => 0x80,
        "SET" => 0xC0,
        _ => { return Err(format!("Invalid use of encode_cbprefix_bit_op: '{}'", mnemonic)); }
    };
    
    match params.as_slice() {
        [Immediate(b), arg] => {
            if let Some(reg_code) = encode_8bit_reg(arg) {
                if *b < 8 {
                    Ok(vec![0xCB, base_code | ((*b as u8) << 3) | reg_code])
                } else {
                    Err(format!("Bit index for {} op must be between 0 and 7, got {}", mnemonic, *b))
                }
            } else {
                Err(format!("Invalid register for {} op, got {:?}", mnemonic, arg))
            }
        }
        _ => Err(format!("Expected a bit and a register for {} op, got {:?}", mnemonic, params))
    }
}

fn encode_cbprefix_rotate_op(mnemonic: &str, params: Vec<Argument>) -> Decoded {
    let base_code: u8 = match mnemonic {
        "RLC" => 0x00,
        "RRC" => 0x08,
        "RL" => 0x10,
        "RR" => 0x18,
        "SLA" => 0x20,
        "SRA" => 0x28,
        "SWAP" => 0x30,
        "SRL" => 0x38,
        _ => { return Err(format!("Invalid use of encode_cbprefix_rotate_op: '{}'", mnemonic)); }
    };
    
    match params.as_slice() {
        [arg] => {
            if let Some(reg_code) = encode_8bit_reg(arg) {
                Ok(vec![0xCB, base_code | reg_code])
            } else {
                Err(format!("Expected a register for {} op, got {:?}", mnemonic, arg))
            }
        }
        _ => Err(format!("Expected a register for {} op, got {:?}", mnemonic, params))
    }
}

fn encode_rst_op(params: Vec<Argument>) -> Decoded {
    match params.as_slice() {
        [Immediate(val)] => {
            match *val {
                0x00 |
                0x08 |
                0x10 |
                0x18 |
                0x20 |
                0x28 |
                0x30 |
                0x38 => Ok(vec![0xC7 | (*val as u8)]),
                _ => Err(format!("RST operation expectes one of $00, $08, $10, $18, $20, $28, $30 or $38, got {}", val))
            }
        }
        _ => Err(format!("RST operation expectes one of $00, $08, $10, $18, $20, $28, $30 or $38, got {:?}", params))
    }
}

fn encode_call_op(params: Vec<Argument>, label_addresses: &HashMap<String, u16>) -> Decoded {
    match params.as_slice() {
        [arg] => {
            let addr = encode_label_or_address(arg, label_addresses)?;
            let (lo, hi) = split_bytes(addr);
            Ok(vec![0xCD, lo, hi])
        }
        [Identifier(cond), arg] => {
            if let Some(cond_code) = encode_condition(cond) {
                let addr = encode_label_or_address(arg, label_addresses)
                    .map_err(|msg| format!("CALL operation: invalid address: {}", msg))?;
                let (lo, hi) = split_bytes(addr);
                Ok(vec![0xC4 | (cond_code << 3), lo, hi])
            } else {
                Err(format!("CALL operation expected a condition first, got {}", cond))
            }
        }
        _ => Err(format!("CALL operation expected an optional condition and an address, got {:?}", params))
    }
}

fn encode_ret_op(params: Vec<Argument>) -> Decoded {
    match params.as_slice() {
        [] => Ok(vec![0xC9]),
        [Identifier(cond)] => {
            if let Some(cond_code) = encode_condition(cond) {
                Ok(vec![0xC0 | (cond_code << 3)])
            } else {
                Err(format!("RET operation expected a condition, got {}", cond))
            }
        }
        _ => Err(format!("RET operation expected an optional condition, got {:?}", params))
    }
}

fn encode_ldhl_op(params: Vec<Argument>) -> Decoded {
    match params.as_slice() {
        [Identifier(sp), ImmediateRelative(val)] if sp == "SP" => {
            Ok(vec![0xF8, *val as u8])
        }
        _ => Err(format!("LDHL expects 'SP, rel8', got {:?}", params))
    }
}

fn encode_jr_op(params: Vec<Argument>, addr: u16, label_addresses: &HashMap<String, u16>) -> Decoded {
    match params.as_slice() {
        [ImmediateRelative(val)] => Ok(vec![0x18, *val as u8]),
        [Identifier(cond), ImmediateRelative(val)] => {
            if let Some(cond_code) = encode_condition(cond) {
                Ok(vec![0x20 | (cond_code << 3), *val as u8])
            }
            else {
                Err(format!("JR operation: {} is not a valid condition (NZ, Z, NC, C)", cond))
            }
        }
        [arg] => {
            let target_addr = encode_label_or_address(arg, label_addresses)
                .map_err(|msg| format!("JR operation: invalid address: {}", msg))?;
            let difference =  (target_addr as i32) - (addr as i32) - 2i32;
            if difference >= -128 && difference < 128 {
                Ok(vec![0x18, difference as u8])
            } else {
                Err(format!("JR operation: address {} too far for relative jump", target_addr))
            }
        }
        [Identifier(cond), arg] => {
            if let Some(cond_code) = encode_condition(cond) {
                let target_addr = encode_label_or_address(arg, label_addresses)
                    .map_err(|msg| format!("JR operation: invalid address: {}", msg))?;
                let difference =  (target_addr as i32) - (addr as i32) - 2i32;
                if difference >= -128 && difference < 128 {
                    Ok(vec![0x20 | (cond_code << 3), difference as u8])
                } else {
                    Err(format!("JR operation: address {} too far for relative jump", target_addr))
                }
            }
            else {
                Err(format!("JR operation: {} is not a valid condition (NZ, Z, NC, C)", cond))
            }
        }
        _ => Err(format!("JR operation expects an optional condition and an immediate relative value, got {:?}", params))
    }
}

fn encode_ldi_op(params: Vec<Argument>) -> Decoded {
    match params.as_slice() {
        [Indirect(IndirectId(hl)), Identifier(a)] if hl=="HL" && a=="A" => {
            Ok(vec![0x22])
        }
        [Identifier(a), Indirect(IndirectId(hl))] if hl=="HL" && a=="A" => {
            Ok(vec![0x2A])
        }
        _ => Err(format!("LDI operation expected 'A, (HL)' or '(HL), A', got {:?}", params))
    }
}

fn encode_ldd_op(params: Vec<Argument>) -> Decoded {
    match params.as_slice() {
        [Indirect(IndirectId(hl)), Identifier(a)] if hl=="HL" && a=="A" => {
            Ok(vec![0x32])
        }
        [Identifier(a), Indirect(IndirectId(hl))] if hl=="HL" && a=="A" => {
            Ok(vec![0x3A])
        }
        _ => Err(format!("LDD operation expected 'A, (HL)' or '(HL), A', got {:?}", params))
    }
}

fn encode_push_op(params: Vec<Argument>) -> Decoded {
    match params.as_slice() {
        [arg] => {
            if let Some(reg_code) = encode_16bit_reg(arg) {
                Ok(vec![0xC5 | (reg_code << 4)])
            } else {
                Err(format!("Expected argument for PUSH to be one of BC, DE, SP or HL, got {:?}", arg))
            }
        }
        _ => Err(format!("Expected one argument for PUSH, got {:?}", params))
    }
}

fn encode_pop_op(params: Vec<Argument>) -> Decoded {
    match params.as_slice() {
        [arg] => {
            if let Some(reg_code) = encode_16bit_reg(arg) {
                Ok(vec![0xC1 | (reg_code << 4)])
            } else {
                Err(format!("Expected argument for POP to be one of BC, DE, SP or HL, got {:?}", arg))
            }
        }
        _ => Err(format!("Expected one argument for POP, got {:?}", params))
    }
}

fn encode_ldh_op(params: Vec<Argument>) -> Decoded {
    match params.as_slice() {
        [Indirect(IndirectImmediate(val)), Identifier(a)] if a == "A" => {
            if *val >= 256 {
                Err(format!("Indirect value for LDH must be an 8 bit value, got {}", *val))
            } else {
                Ok(vec![0xE0, *val as u8])
            }
        }
        [Identifier(a), Indirect(IndirectImmediate(val))] if a == "A" => {
            if *val >= 256 {
                Err(format!("Indirect value for LDH must be an 8 bit value, got {}", *val))
            } else {
                Ok(vec![0xF0, *val as u8])
            }
        }
        _ => Err(format!("Expected 'A, (8 bit value)' or '(8 bit value), A' for LDH operation, got {:?}", params))
    }
}

fn encode_dec_op(params: Vec<Argument>) -> Decoded {
    match params.as_slice() {
        [arg] => {
            if let Some(reg_code) = encode_8bit_reg(arg) {
                Ok(vec![0x05 | (reg_code << 3)])
            } else if let Some(reg_code) = encode_16bit_reg(arg) {
                Ok(vec![0x0B | (reg_code << 4)])
            } else {
                Err(format!("DEC operation expected one of B, C, D, H, L, (HL), BC, DE, HL, SP, got '{:?}'", arg))
            }
        }
        _ => Err(format!("DEC operation expects one argument"))
    }
}

fn encode_inc_op(params: Vec<Argument>) -> Decoded {
    match params.as_slice() {
        [arg] => {
            if let Some(reg_code) = encode_8bit_reg(arg) {
                Ok(vec![0x04 | (reg_code << 3)])
            } else if let Some(reg_code) = encode_16bit_reg(arg) {
                Ok(vec![0x03 | (reg_code << 4)])
            } else {
                Err(format!("INC operation expected one of B, C, D, H, L, (HL), BC, DE, HL, SP, got '{:?}'", arg))
            }
        }
        _ => Err(format!("INC operation expects one argument"))
    }
}

fn encode_adc_sbc_op(mnemonic: &str, params: Vec<Argument>) -> Decoded {
    let opcode: u8 = match mnemonic {
        "ADC" => 0x88,
        "SBC" => 0x98,
        _ => panic!("Bad call to encode_adc_sbc_op")
    };
    
    match params.as_slice() {
        [Identifier(a), Immediate(val)] if a == "A" => {
            if *val >= 256 {
                Err(format!("Immediate value too big for {}: {}", mnemonic, val))
            } else {
                Ok(vec![opcode | 0x46, *val as u8])
            }
        }
        [Identifier(a), arg] if a == "A" => {
            if let Some(r) = encode_8bit_reg(arg) {
                Ok(vec![opcode | r])
            } else {
                Err(format!("Invalid second argument to {}: {:?}", mnemonic, arg))
            }
        }
        _ => Err(format!("Invalid format for {}: expected A followed by 8 bit register or immediate value, got {:?}", mnemonic, params))
    }
}

fn encode_sub_logical_op(mnemonic: &str, params: Vec<Argument>) -> Decoded {
    let opcode: u8 = match mnemonic {
        "SUB" => 0x90,
        "AND" => 0xA0,
        "XOR" => 0xA8,
        "OR"  => 0xB0,
        "CP"  => 0xB8,
        _ => panic!("Bad call to encode_sub_logical_op")
    };
    
    match params.as_slice() {
        [Immediate(val)] => {
            if *val >= 256 {
                Err(format!("Immediate value too big for {}: {}", mnemonic, val))
            } else {
                Ok(vec![opcode | 0x46, *val as u8])
            }
        }
        [arg]  => {
            if let Some(r) = encode_8bit_reg(arg) {
                Ok(vec![opcode | r])
            } else {
                Err(format!("Invalid argument to {}: {:?}", mnemonic, arg))
            }
        }
        _ => Err(format!("Invalid format for {}: expected 8 bit register or immediate value, got {:?}", mnemonic, params))
    }
}

fn encode_add_op(params: Vec<Argument>) -> Decoded {
    match params.as_slice() {
        [Identifier(a), Immediate(val)] if a == "A" => {
            if *val >=256 {
                Err(format!("Value too big to add to A: {}", val))
            } else {
                Ok(vec![0xC6, *val as u8])
            }
        }
        [Identifier(a), arg] if a == "A" => {
            let reg_code = encode_8bit_reg(arg)
                .ok_or_else(|| format!("Expected one of B, C, D, H, L, (HL) or A to add into A, got {:?}", arg))?;
            Ok(vec![0x80 | reg_code])
        }
        [Identifier(hl), arg] if hl == "HL" => {
            let reg_code = encode_16bit_reg(arg)
                .ok_or_else(|| format!("Expected one of BC, DE, HL or SP to add into HL, got {:?}", arg))?;
            Ok(vec![0x09 | (reg_code << 4)])
        }
        [Identifier(sp), ImmediateRelative(rel)] if sp == "SP" => {
            Ok(vec![0xE8, *rel as u8])
        }
        _ => Err(format!("Invalid parameters for ADD operation: {:?}", params))
    }
}

fn encode_jp_op(params: Vec<Argument>, label_addresses: &HashMap<String, u16>) -> Decoded {
    match params.as_slice() {
        [Indirect(IndirectId(id))] if id == "HL" => Ok(vec![0xE9]),
        [Identifier(id), arg2] => {
            let cond = encode_condition(&id).ok_or_else(|| format!(
                "Expected first argument of JP (namely, '{}') to be a condition", id
            ))?;
            let (lo, hi) = split_bytes(encode_label_or_address(arg2, label_addresses)
                .map_err(|msg| format!("JP operation, invalid address: {}", msg))?);
            Ok(vec![0xC2 | (cond << 3), lo, hi])
        }
        [arg] => {
            let (lo, hi) = split_bytes(encode_label_or_address(arg, label_addresses)
                .map_err(|msg| format!("JP operation, invalid address: {}", msg))?);
            Ok(vec![0xC3, lo, hi])
        }
        _ => Err(format!("Invalid parameters for JP operation: {:?}", params))
    }
}

fn encode_ld_op(params: Vec<Argument>) -> Decoded {
    match params.as_slice() {
        [reg, Immediate(val)] => {
            if let Some(reg_code) = encode_8bit_reg(reg) {
                if *val >= 256 {
                    Err(format!("Expected an 8 bit value for LD operation, got {}", val))
                } else {
                    Ok(vec![0x06 | (reg_code << 3), *val as u8])
                }
            } else if let Some(reg_code) = encode_16bit_reg(reg) {
                let (lo, hi) = split_bytes(*val);
                Ok(vec![0x01 | (reg_code << 4), lo, hi])
            } else {
                Err(format!(
                    "Expected a register where to load immediate value, got '{:?}'", reg
                ))
            }
        }
        [Indirect(ind), Identifier(a)] if a == "A" => {
            match ind {
                IndirectId(reg) => {
                    if reg == "C" {
                        Ok(vec![0xE2])
                    } else if reg == "BC" {
                        Ok(vec![0x02])
                    } else if reg == "DE" {
                        Ok(vec![0x12])
                    } else {
                        Err(format!("Expected indirect register to be one of C, BC or DE, got {}", reg))
                    }
                }
                IndirectImmediate(val) => {
                    let (lo, hi) = split_bytes(*val);
                    Ok(vec![0xEA, lo, hi])
                }
            }
        }
        [Identifier(a), Indirect(ind)] if a == "A" => {
            match ind {
                IndirectId(reg) => {
                    if reg == "C" {
                        Ok(vec![0xF2])
                    } else if reg == "BC" {
                        Ok(vec![0x0A])
                    } else if reg == "DE" {
                        Ok(vec![0x1A])
                    } else {
                        Err(format!("Expected indirect register to be one of C, BC or DE, got {}", reg))
                    }
                }
                IndirectImmediate(val) => {
                    let (lo, hi) = split_bytes(*val);
                    Ok(vec![0xFA, lo, hi])
                }
            }
        }
        [Indirect(IndirectImmediate(val)), Identifier(sp)] if sp == "SP" => {
            let (lo, hi) = split_bytes(*val);
            Ok(vec![0x08, lo, hi])
        }
        [Identifier(sp), Identifier(hl)] if sp=="SP" && hl=="HL" => {
            Ok(vec![0xF9])
        }
        [arg1, arg2] => {
            match (encode_8bit_reg(arg1), encode_8bit_reg(arg2)) {
                (Some(r1), Some(r2)) => {
                    if r1 == 0b0110 && r2 == 0b0110 {
                        Err(format!("Can't load (HL) into (HL), invalid operation"))
                    } else {
                        Ok(vec![0x40 | (r1 << 3) | r2])
                    }
                }
                _ => Err(format!("Invalid parameters for LD operation: {:?}", params))
            }
        }
        _ => Err(format!("Invalid parameters for LD operation: {:?}", params))
    }
}

fn encode_label_or_address(arg: &Argument, label_addresses: &HashMap<String, u16>) -> Result<u16, String> {
    match arg {
        Immediate(val) => Ok(*val),
        Label(l) => {
            label_addresses.get(l).map(|&v| v)
                .ok_or_else(|| format!("Undefined label '{}'", l))
        }
        _ => Err(format!("Expected literal address or label, got '{:?}'", arg))
    }
}

fn split_bytes(b: u16) -> (u8, u8) {
    (b as u8, (b >> 8) as u8)
}

fn encode_condition(argument: &str) -> Option<u8> {
    Some(match argument {
        "NZ" => 0b00,
        "Z"  => 0b01,
        "NC" => 0b10,
        "C"  => 0b11,
        _ => { return None; }
    })
}

fn encode_16bit_reg(argument: &Argument) -> Option<u8> {
    if let Identifier(id) = argument {
        Some(match id.as_str() {
            "BC" => 0b00,
            "DE" => 0b01,
            "HL" => 0b10,
            "SP" => 0b11,
            _ => { return None; }
        })
    } else {
        None
    }
}

fn encode_8bit_reg(argument: &Argument) -> Option<u8> {
    Some(match argument {
        Identifier(id) => {
            match id.as_str() {
                "B" => 0b000,
                "C" => 0b001,
                "D" => 0b010,
                "E" => 0b011,
                "H" => 0b100,
                "L" => 0b101,
                "A" => 0b111,
                _ => { return None; }
            }
        }
        Indirect(IndirectId(id)) => {
            match id.as_str() {
                "HL" => 0b0110,
                _ => { return None; }
            }
        }
        _ => { return None; }
    })
}

fn encode_no_argument_op(mnemonic: &str) -> Decoded {
    Ok(match mnemonic {
        "NOP" => vec![0x00],
        "STOP" => vec![0x10],
        "HALT" => vec![0x76],
        "DI" => vec![0xF3],
        "EI" => vec![0xFB],
        "DAA" => vec![0x27],
        "SCF" => vec![0x37],
        "CPL" => vec![0x2F],
        "CCF" => vec![0x3F],
        "RLCA" => vec![0x07],
        "RRCA" => vec![0x0F],
        "RLA" => vec![0x17],
        "RRA" => vec![0x1F],
        "RETI" => vec![0xD9],
        _ => { return Err(format!("Operation '{}' needs argument(s)", mnemonic)); }
    })
}

#[cfg(test)]
mod test {
    use crate::parser::{Op, Argument};
    use crate::parser::Argument::*;
    use crate::parser::Indirect::*;
    
    use super::*;
    
    #[test]
    fn test_ld_encoding() {
        assert_eq!(
            encode_ld_op(vec![
                Identifier("SP".to_string()),
                Identifier("HL".to_string())
            ]),
            Ok(vec![0xF9])
        );
        
        assert_eq!(
            encode_ld_op(vec![
                Identifier("HL".to_string()),
                Immediate(0xBEEF)
            ]),
            Ok(vec![0x21, 0xEF, 0xBE])
        );
        
        assert_eq!(
            encode_ld_op(vec![
                Indirect(IndirectId("DE".to_string())),
                Identifier("A".to_string())
            ]),
            Ok(vec![0x12])
        );
        
        assert_eq!(
            encode_ld_op(vec![
                Indirect(IndirectId("C".to_string())),
                Identifier("A".to_string())
            ]),
            Ok(vec![0xE2])
        );
        
        assert_eq!(
            encode_ld_op(vec![
                Identifier("A".to_string()),
                Indirect(IndirectId("DE".to_string()))
            ]),
            Ok(vec![0x1A])
        );
        
        assert_eq!(
            encode_ld_op(vec![
                Identifier("A".to_string()),
                Indirect(IndirectId("C".to_string()))
            ]),
            Ok(vec![0xF2])
        );
        
        assert_eq!(
            encode_ld_op(vec![
                Identifier("A".to_string()),
                Indirect(IndirectImmediate(0xBEEF))
            ]),
            Ok(vec![0xFA, 0xEF, 0xBE])
        );
        
        assert_eq!(
            encode_ld_op(vec![
                Indirect(IndirectImmediate(0xBEEF)),
                Identifier("SP".to_string())
            ]),
            Ok(vec![0x08, 0xEF, 0xBE])
        );
        
        assert_eq!(
            encode_ld_op(vec![
                Identifier("D".to_string()),
                Identifier("E".to_string())
            ]),
            Ok(vec![0x53])
        );
        
        assert_eq!(
            encode_ld_op(vec![
                Identifier("A".to_string()),
                Identifier("C".to_string())
            ]),
            Ok(vec![0x79])
        );
        
        assert!(
            encode_ld_op(vec![
                Indirect(IndirectId("HL".to_string())),
                Indirect(IndirectId("HL".to_string()))
            ]).is_err()
        )
    }
    
    #[test]
    fn test_jp_encoding() {
        let mut lab = HashMap::<String, u16>::new();
        lab.insert("test".to_string(), 0xBEEF);
        
        assert_eq!(
            encode_jp_op(vec![
                Immediate(0xDEAD)
            ], &lab),
            Ok(vec![0xC3, 0xAD, 0xDE])
        );
        
        assert_eq!(
            encode_jp_op(vec![
                Identifier("NZ".to_string()),
                Immediate(0xDEAD)
            ], &lab),
            Ok(vec![0xC2, 0xAD, 0xDE])
        );
        
        assert_eq!(
            encode_jp_op(vec![
                Identifier("C".to_string()),
                Label("test".to_string())
            ], &lab),
            Ok(vec![0xDA, 0xEF, 0xBE])
        );
        
        assert_eq!(
            encode_jp_op(vec![
                Indirect(IndirectId("HL".to_string()))
            ], &lab),
            Ok(vec![0xE9])
        );
        
        assert!(
            encode_jp_op(vec![
                Identifier("NC".to_string()),
                Label("undefined".to_string())
            ], &lab).is_err()
        );
    }
    
    #[test]
    fn test_arithmetic_logical_encoding() {
        assert_eq!(
            encode_add_op(vec![
                Identifier("A".to_string()),
                Identifier("L".to_string())
            ]),
            Ok(vec![0x85])
        );
        
        assert!(
            encode_add_op(vec![
                Identifier("C".to_string())
            ]).is_err()
        );
        
        assert_eq!(
            encode_adc_sbc_op("ADC", vec![
                Identifier("A".to_string()),
                Identifier("C".to_string())
            ]),
            Ok(vec![0x89])
        );
        
        assert_eq!(
            encode_adc_sbc_op("SBC", vec![
                Identifier("A".to_string()),
                Identifier("E".to_string())
            ]),
            Ok(vec![0x9B])
        );
        
        assert!(
            encode_adc_sbc_op("ADC", vec![
                Identifier("C".to_string())
            ]).is_err()
        );
        
        assert_eq!(
            encode_sub_logical_op("SUB", vec![
                Identifier("D".to_string())
            ]),
            Ok(vec![0x92])
        );
        
        assert_eq!(
            encode_sub_logical_op("AND", vec![
                Indirect(IndirectId("HL".to_string()))
            ]),
            Ok(vec![0xA6])
        );
        
        assert_eq!(
            encode_sub_logical_op("XOR", vec![
                Identifier("A".to_string())
            ]),
            Ok(vec![0xAF])
        );
        
        assert_eq!(
            encode_sub_logical_op("OR", vec![
                Identifier("B".to_string())
            ]),
            Ok(vec![0xB0])
        );
        
        assert_eq!(
            encode_sub_logical_op("CP", vec![
                Identifier("C".to_string())
            ]),
            Ok(vec![0xB9])
        );
        
        assert!(
            encode_sub_logical_op("AND", vec![
                Identifier("A".to_string()),
                Identifier("C".to_string())
            ]).is_err()
        );
    }
    
    #[test]
    fn test_inc_dec_encoding() {
        assert_eq!(
            encode_inc_op(vec![
                Identifier("D".to_string())
            ]),
            Ok(vec![0x14])
        );
        
        assert_eq!(
            encode_inc_op(vec![
                Indirect(IndirectId("HL".to_string()))
            ]),
            Ok(vec![0x34])
        );
        
        assert_eq!(
            encode_inc_op(vec![
                Identifier("SP".to_string())
            ]),
            Ok(vec![0x33])
        );
        
        assert_eq!(
            encode_dec_op(vec![
                Identifier("H".to_string())
            ]),
            Ok(vec![0x25])
        );
        
        assert_eq!(
            encode_dec_op(vec![
                Indirect(IndirectId("HL".to_string()))
            ]),
            Ok(vec![0x35])
        );
        
        assert_eq!(
            encode_dec_op(vec![
                Identifier("HL".to_string())
            ]),
            Ok(vec![0x2B])
        );
    }
}