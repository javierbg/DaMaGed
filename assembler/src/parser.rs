// Based on the Tiny parser by Markus Siemes
// https://github.com/msiemens/rust-tinyasm/blob/master/src/assembler/parser/mod.rs
// Licensed under the MIT license

use std::collections::LinkedList;
use crate::util::fatal;
use crate::lexer::{Lexer, FileLexer, Token};

pub use crate::lexer::SourceLocation;

pub type Program = Vec<Statement>;

#[derive(Debug, PartialEq)]
pub enum Statement {
    Section(u16),
    Label(String),
    Operation(Op)
}

#[derive(Debug, PartialEq)]
pub struct Op {
    pub mnemonic: String,
    pub params: Vec<Argument>
}

#[derive(Debug, PartialEq)]
pub enum Argument {
    Label(String),
    Identifier(String),
    Immediate(u16),
    ImmediateRelative(i8),
    Indirect(Indirect),
}

#[derive(Debug, PartialEq)]
pub enum Indirect {
    IndirectImmediate(u16),
    IndirectId(String),
}

type Identifier = String;
type Label = String;
type Immediate = u16;
type ImmediateRel = i8;

pub struct Parser<'a> {
    location: SourceLocation,
    token: Token<'a>,
    buffer: LinkedList<Token<'a>>,
    lexer: Box<Lexer<'a> + 'a>
}

impl<'a> Parser<'a> {
    pub fn new(source: &'a str, file: &str) -> Parser<'a> {
        Parser::with_lexer(Box::new(FileLexer::new(source, file)))
    }

    pub fn with_lexer(mut lx: Box<Lexer<'a> + 'a>) -> Parser {
        Parser {
            token: lx.next_token(),
            location: lx.get_source(),
            buffer: LinkedList::new(),
            lexer: lx
        }
    }

    pub fn parse(&mut self) -> Program {
        let mut source = vec![];

        // debug!("Starting parsing");

        while self.token != Token::EOF {
            source.push(self.parse_statement());
        }

        // debug!("Parsing finished");

        source
    }


    // --- Error handling -------------------------------------------------------

    fn fatal(&self, msg: String) -> ! {
        fatal(msg, &self.location);
    }

    fn unexpected_token(&self, tok: &Token, expected: Option<&'static str>) -> ! {
        match expected {
            Some(ex) => self.fatal(format!("unexpected token: `{}`, expected {}", tok, ex)),
            None => self.fatal(format!("unexpected token: `{}`", tok))
        }
    }


    // --- Token processing -----------------------------------------------------

    fn update_location(&mut self) -> SourceLocation {
        self.location = self.lexer.get_source();
        self.location.clone()
    }

    fn bump(&mut self) {
        self.token = match self.buffer.pop_front() {
            Some(tok) => tok,
            None => self.lexer.next_token()
        };
    }

    fn eat(&mut self, tok: &Token) -> bool {
        if self.token == *tok {
            self.bump();
            true
        } else {
            false
        }
    }

    fn expect(&mut self, tok: &Token) {
        if !self.eat(tok) {
            self.fatal(format!("expected `{}`, found `{}`", tok, self.token))
        }
    }

    fn look_ahead<F, R>(&mut self, distance: usize, f: F) -> R
        where F: Fn(&Token) -> R {
        if self.buffer.len() < distance {
            for _ in 0 .. distance - self.buffer.len() {
                self.buffer.push_back(self.lexer.next_token());
            }
        }

        f(self.buffer.iter().nth(distance - 1).unwrap())
    }

    // --- Actual parsing -------------------------------------------------------

    fn token_is_argument(&mut self) -> bool {
        match self.token {
            Token::IDENT(_) => {
                // Check if it is a label first
                if self.look_ahead(1, |t| *t == Token::COLON) {
                    false // If you see a : up ahead, it's a label
                } else {
                    true
                }
            },
            Token::UNSIGNED(_) | Token::SIGNED(_) |
            Token::COLON | Token::LPAREN => true,
            _ => false
        }
    }

    // --- Parsing: Single tokens -----------------------------------------------

    fn parse_ident(&mut self) -> Identifier {
        let ident = match self.token {
            Token::IDENT(id) => id.to_owned(),
            _ => self.unexpected_token(&self.token, Some("a identifier"))
        };
        self.bump();

        ident
    }
    
    fn parse_immediate(&mut self) -> Immediate {
        let imm = match self.token {
            Token::UNSIGNED(u) => u,
            _ => self.unexpected_token(&self.token, Some("a literal value"))
        };
        self.bump();
        
        imm
    }
    
    fn parse_immediate_rel(&mut self) -> ImmediateRel {
        let imm = match self.token {
            Token::SIGNED(s) => s,
            _ => self.unexpected_token(&self.token, Some("a relative literal value"))
        };
        self.bump();
        
        imm
    }

    fn parse_label(&mut self) -> Label {
        self.expect(&Token::COLON);
        self.parse_ident()
    }

    fn parse_indirect(&mut self) -> Indirect {
        self.expect(&Token::LPAREN);
        let ind = match self.token {
            Token::IDENT(_) => Indirect::IndirectId(self.parse_ident()),
            Token::UNSIGNED(_) => Indirect::IndirectImmediate(self.parse_immediate()),
            _ => self.unexpected_token(&self.token, Some("a register or an immediate value"))
        };
        self.expect(&Token::RPAREN);

        ind
    }
    
    

    fn parse_argument(&mut self) -> Argument {
        self.update_location();

        match self.token {
            Token::UNSIGNED(_) => Argument::Immediate(self.parse_immediate()),
            Token::SIGNED(_)   => Argument::ImmediateRelative(self.parse_immediate_rel()),
            Token::IDENT(_)    => Argument::Identifier(self.parse_ident()),
            Token::LPAREN      => Argument::Indirect(self.parse_indirect()),
            Token::COLON       => Argument::Label(self.parse_label()),
            _ => self.unexpected_token(&self.token, Some("an argument"))
        }
    }

    // ---- Parsing: Expressions ------------------------------------------------

    fn parse_label_def(&mut self) -> Statement {
        self.update_location();

        let label = self.parse_ident();
        self.expect(&Token::COLON);

        Statement::Label(label)
    }

    fn parse_operation(&mut self) -> Statement {
        self.update_location();

        let mn = if let Token::MNEMONIC(mn) = self.token {
            mn.to_uppercase()
        } else {
            self.unexpected_token(&self.token, Some("a mnemonic"))
        };

        self.bump();

        let mut args = vec![];
        if self.token_is_argument() {
            args.push(self.parse_argument());
            while self.eat(&Token::COMMA) {
                args.push(self.parse_argument());
            }
        }

        Statement::Operation(Op {
            mnemonic: mn,
            params: args,
        })
    }
    
    fn parse_section(&mut self) -> Statement {
        self.expect(&Token::SECTION);
        let addr = self.parse_immediate();
        Statement::Section(addr)
    }

    fn parse_statement(&mut self) -> Statement {
        match self.token {
            Token::SECTION => self.parse_section(),
            Token::IDENT(_) => self.parse_label_def(),
            Token::MNEMONIC(_) => self.parse_operation(),
            _ => self.unexpected_token(&self.token, Some("a statement"))
        }
    }
}

#[cfg(test)]
mod tests {
    use std::borrow::ToOwned;
    use std::rc::Rc;

    use crate::lexer::{Token, Lexer};
    use crate::lexer::Token::*;

    use super::*;

    fn parse<'a, F, T>(toks: Vec<Token<'a>>, f: F) -> T where F: Fn(&mut Parser<'a>) -> T {
        f(&mut Parser::with_lexer(Box::new(toks) as Box<Lexer>))
    }

    #[test]
    fn test_statements() {
        assert_eq!(
            parse(
                vec![MNEMONIC("RST"),  UNSIGNED(0x10),
                     MNEMONIC("HALT")],
                |p| p.parse()
            ),
            vec![
                Statement::Operation(Op {
                    mnemonic: "RST".to_string(),
                    params: vec![Argument::Immediate(0x10)]
                }),
                Statement::Operation(Op {
                    mnemonic: "HALT".to_string(),
                    params: vec![]
                }),
            ]
        )
    }

    #[test]
    fn test_label_def() {
        assert_eq!(
            parse(vec![IDENT("lbl"), COLON],
                  |p| p.parse_statement()),
            Statement::Label("lbl".to_string())
        )
    }

    #[test]
    fn test_operation() {
        assert_eq!(
            parse(vec![MNEMONIC("LD"), IDENT("A"), COMMA, IDENT("B")],
                  |p| p.parse_statement()),
            Statement::Operation(Op {
                mnemonic: "LD".to_string(),
                params: vec![Argument::Identifier("A".to_string()),
                             Argument::Identifier("B".to_string())]
            })
        )
    }
    
    #[test]
    fn test_section() {
        assert_eq!(
            parse(vec![SECTION, UNSIGNED(0x150)],
                  |p| p.parse_statement()),
            Statement::Section(0x150)
        )
    }

    #[test]
    fn test_immediate() {
        assert_eq!(
            parse(vec![UNSIGNED(0)],
                  |p| p.parse_argument()),
            Argument::Immediate(0)
        );

        assert_eq!(
            parse(vec![SIGNED(0)],
                  |p| p.parse_argument()),
            Argument::ImmediateRelative(0)
        );
    }

    #[test]
    fn test_label() {
        assert_eq!(
            parse(vec![COLON, IDENT("asd")],
                  |p| p.parse_argument()),
            Argument::Label("asd".to_string())
        )
    }
}
