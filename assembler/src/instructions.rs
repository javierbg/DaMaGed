use crate::parser::{Op, Program};
use crate::parser::Statement;
use crate::parser::Argument::*;
use crate::parser::Indirect::*;
use crate::util::fatal_generic;
use crate::encoding::encode_op;
use std::collections::HashMap;

#[derive(PartialEq, Debug)]
pub struct Section {
    pub start_address: u16,
    pub operations: Vec<Op>,
    pub label_indexes: HashMap<String, usize>,
}

#[derive(PartialEq, Debug)]
pub struct AddressedSection {
    pub operations: Vec<Op>,
    pub start_address: u16,
    pub length: u16,
    pub label_addresses: HashMap<String, u16>,
}

const MAX_BINARY_SIZE: usize = 0x8000; // 32K

pub fn build_binary(prog: Program) -> Result<Box<[u8]>, String> {
    let mut addressed_sections: Vec<AddressedSection> = split_sections(prog).into_iter()
        .map(|s| assign_addresses(s))
        .collect();
    
    check_sections_dont_overlap(&mut addressed_sections)?;
    let encoded_sections_result: Result<Vec<(u16, Vec<u8>)>, String> = 
        addressed_sections.into_iter()
        .map(|s| encode_section(s))
        .collect();
    let encoded_sections = encoded_sections_result?;
    
    let mut binary: [u8; MAX_BINARY_SIZE] = [0; MAX_BINARY_SIZE];
    for (sec_start, data) in encoded_sections.into_iter() {
        for (rel_idx, val) in data.into_iter().enumerate() {
            let idx = (sec_start as usize) + rel_idx;
            // Check bounds
            if idx >= MAX_BINARY_SIZE {
                return Err(format!(
                    "Exceeded max binary size ({} bytes) in section starting at adddress {:04X}",
                    MAX_BINARY_SIZE, sec_start
                ));
            }
            binary[idx] = val;
        }
    }
    
    Ok(Box::new(binary))
}

fn check_sections_dont_overlap<'a>(sections: &'a mut [AddressedSection]) -> Result<(), String> {
    sections.sort_by(|s1, s2| {
        s1.start_address.partial_cmp(&s2.start_address).unwrap()
    });
    
    sections.iter().zip(sections.iter().skip(1)).try_for_each(|(s1, s2)| {
        if (s1.start_address + s1.length) <= s2.start_address {
            Ok(())
        } else {
            Err(format!(
                "Section starting at address {:04X} overlaps with section at address {:04X}",
                s1.start_address, s2.start_address
            ))
        }
    })
}

// Returns a tuple with the start address of the section and
// the bytes that form it
fn encode_section(ad_section: AddressedSection) -> Result<(u16, Vec<u8>), String> {
    let start_address = ad_section.start_address;
    let mut curr_address = start_address;
    let mut codes: Vec<u8> = vec![];
    
    for op in ad_section.operations {
        let encoded_op = encode_op(op, curr_address, &ad_section.label_addresses)?;
        curr_address += encoded_op.len() as u16;
        codes.extend(encoded_op.iter());
    }
    
    Ok((start_address, codes))
}

fn assign_addresses(section: Section) -> AddressedSection {
    let mut addressed_operations: Vec<u16> = vec![];
    
    let start_address = section.start_address;
    let mut curr_address = section.start_address;
    for op in section.operations.iter() {
        let op_len = operation_length(op);
        let increment = match op_len {
            Some(len) => len as u16,
            None => fatal_generic(format!("Invalid operation {:?}", op)),
        };
        addressed_operations.push(curr_address);
        curr_address += increment;
    }
    
    let mut label_addresses = HashMap::<String, u16>::new();
    section.label_indexes.iter().for_each(|(label, idx)| {
        let addr = addressed_operations[*idx];
        label_addresses.insert(label.to_string(), addr);
    });
    
    AddressedSection {
        operations: section.operations,
        label_addresses: label_addresses,
        start_address: start_address,
        length: curr_address - start_address,
    }
}

fn split_sections(prog: Program) -> Vec<Section> {
    let mut curr_section_start: u16 = match prog.get(0) {
        Some(Statement::Section(n)) => n.clone(),
        _ => fatal_generic("Expected first statement to be a SECTION declaration".to_string())
    };
    let mut sections: Vec<Section> = vec![];
    let mut curr_labels = HashMap::<String, usize>::new();
    let mut curr_operations: Vec<Op> = vec![];
    let mut acc_labels: Vec<String> = vec![];
    
    let mut op_index = 0;
    for stmt in prog.into_iter().skip(1) {
        match stmt {
            Statement::Label(l) => acc_labels.push(l),
            Statement::Operation(o) => {
                curr_operations.push(o);
                for label in acc_labels.iter() {
                    curr_labels.insert(label.to_string(), op_index);
                }
                acc_labels.clear();
                op_index += 1;
            },
            Statement::Section(addr) => {
                sections.push(Section {
                    start_address: curr_section_start,
                    operations: curr_operations,
                    label_indexes: curr_labels,
                });
                curr_section_start = addr;
                curr_operations = vec![];
                curr_labels = HashMap::<String, usize>::new();
                op_index = 0;
            }
        }
    }
    
    // Save last section
    sections.push(Section {
        start_address: curr_section_start,
        operations: curr_operations,
        label_indexes: curr_labels,
    });
    
    sections
}

// Returns the length in bytes of an operation, assumes
// that it is semantically correct
fn operation_length(op: &Op) -> Option<usize> {
    Some(match op.mnemonic.as_str() {
        "NOP" => 1,
        "STOP" => 2,
        "DI" | "EI" => 1,
        "HALT" => 1,
        "CALL" => 3,
        "PUSH" | "POP" => 1,
        "RET" | "RETI" => 1,
        "RST" => 1,
        "LDH" => 2,
        "LDHL" => 2,
        "INC" | "DEC" => 1,
        "DAA" | "SCF" | "CPL" | "CCF" => 1,
        "RLCA" | "RLA" | "RRCA" | "RRA" => 1,
        "RLC" | "RRC" | "RL" | "RR" | "SLA" | "SRA" |
            "SWAP" | "SRL" | "BIT" | "RES" | "SET" => 2,
        "SUB" | "AND" | "OR" | "XOR" | "CP" => {
            if let Some(Immediate(_)) = op.params.get(0) { 2 } else { 1 }
        },
        "ADC" | "SBC" => {
            if let Some(Immediate(_)) = op.params.get(1) { 2 } else { 1 }
        }
        "ADD" => {
            match op.params.get(1) {
                Some(Immediate(_)) | Some(ImmediateRelative(_)) => 2,
                _ => 1
            }
        }
        "JR" => 2,
        "JP" => {
            if let Some(Indirect(IndirectId(reg))) = op.params.get(0){
                if reg == "HL" { 1 } else { 3 }
            } else { 3 }
        },
        "LDD" | "LDI" => 1,
        "LD" => {
            if op.params.len() != 2 { 1 } // Invalid, but should be caught later
            else {
                let arg_pair = (&op.params[0], &op.params[1]);
                match arg_pair {
                    (Indirect(IndirectImmediate(_)), _) => 3, // LD (a16), A
                    (_, Indirect(IndirectImmediate(_))) => 3, // LD A, (a16)
                    (Identifier(_), Identifier(_)) => 1,
                    (Identifier(id), Immediate(_)) => {
                        if id == "BC" || id == "DE" || id == "HL" || id == "SP" {
                            3
                        } else {
                            2
                        }
                    }
                    _ => 1
                }
            }
        },
        _ => { return None; }
    })
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::parser::Op;
    use crate::parser::Argument::*;
    use crate::parser::Indirect::*;

    #[test]
    fn test_ld_op_length() {
        let op = Op {
            mnemonic: "LD".to_string(),
            params: vec![Identifier("B".to_string()),
                         Identifier("C".to_string())]
        };
        assert_eq!(operation_length(&op).unwrap(), 1);

        let op = Op {
            mnemonic: "LD".to_string(),
            params: vec![Identifier("SP".to_string()),
                         Identifier("HL".to_string())]
        };
        assert_eq!(operation_length(&op).unwrap(), 1);

        let op = Op {
            mnemonic: "LD".to_string(),
            params: vec![Identifier("B".to_string()),
                         Immediate(0)]
        };
        assert_eq!(operation_length(&op).unwrap(), 2);

        let op = Op {
            mnemonic: "LD".to_string(),
            params: vec![Identifier("BC".to_string()),
                         Immediate(0)]
        };
        assert_eq!(operation_length(&op).unwrap(), 3);

        let op = Op {
            mnemonic: "LD".to_string(),
            params: vec![Indirect(IndirectImmediate(0)),
                         Identifier("SP".to_string())]
        };
        assert_eq!(operation_length(&op).unwrap(), 3);

        let op = Op {
            mnemonic: "LD".to_string(),
            params: vec![Indirect(IndirectImmediate(0)),
                         Identifier("A".to_string())]
        };
        assert_eq!(operation_length(&op).unwrap(), 3);

        let op = Op {
            mnemonic: "LD".to_string(),
            params: vec![Identifier("A".to_string()),
                         Indirect(IndirectImmediate(0))]
        };
        assert_eq!(operation_length(&op).unwrap(), 3);
    }

    #[test]
    fn test_add_op_length() {
        let op = Op {
            mnemonic: "ADD".to_string(),
            params: vec![Identifier("A".to_string()),
                         Identifier("B".to_string())]
        };
        assert_eq!(operation_length(&op).unwrap(), 1);

        let op = Op {
            mnemonic: "ADD".to_string(),
            params: vec![Identifier("A".to_string()),
                         Indirect(IndirectId("HL".to_string()))]
        };
        assert_eq!(operation_length(&op).unwrap(), 1);

        let op = Op {
            mnemonic: "ADD".to_string(),
            params: vec![Identifier("HL".to_string()),
                         Identifier("SP".to_string())]
        };
        assert_eq!(operation_length(&op).unwrap(), 1);

        let op = Op {
            mnemonic: "ADD".to_string(),
            params: vec![Identifier("A".to_string()),
                         Immediate(0)]
        };
        assert_eq!(operation_length(&op).unwrap(), 2);

        let op = Op {
            mnemonic: "ADD".to_string(),
            params: vec![Identifier("SP".to_string()),
                         Immediate(0)]
        };
        assert_eq!(operation_length(&op).unwrap(), 2);
    }

    #[test]
    fn test_adc_sbc_op_length() {
        let op = Op {
            mnemonic: "ADC".to_string(),
            params: vec![Identifier("A".to_string()),
                         Identifier("B".to_string())]
        };
        assert_eq!(operation_length(&op).unwrap(), 1);

        let op = Op {
            mnemonic: "ADC".to_string(),
            params: vec![Identifier("A".to_string()),
                         Indirect(IndirectId("HL".to_string()))]
        };
        assert_eq!(operation_length(&op).unwrap(), 1);

        let op = Op {
            mnemonic: "ADC".to_string(),
            params: vec![Identifier("A".to_string()),
                         Immediate(0)]
        };
        assert_eq!(operation_length(&op).unwrap(), 2);
    }

    #[test]
    fn test_arith_logic_length() {
        let op = Op {
            mnemonic: "AND".to_string(),
            params: vec![Identifier("B".to_string())]
        };
        assert_eq!(operation_length(&op).unwrap(), 1);

        let op = Op {
            mnemonic: "AND".to_string(),
            params: vec![Indirect(IndirectId("HL".to_string()))]
        };
        assert_eq!(operation_length(&op).unwrap(), 1);

        let op = Op {
            mnemonic: "AND".to_string(),
            params: vec![Immediate(0)]
        };
        assert_eq!(operation_length(&op).unwrap(), 2);
    }

    #[test]
    fn test_jp_op_length() {
        let op = Op {
            mnemonic: "JP".to_string(),
            params: vec![Immediate(0)]
        };
        assert_eq!(operation_length(&op).unwrap(), 3);

        let op = Op {
            mnemonic: "JP".to_string(),
            params: vec![Identifier("NZ".to_string()),
                         Immediate(0)]
        };
        assert_eq!(operation_length(&op).unwrap(), 3);

        let op = Op {
            mnemonic: "JP".to_string(),
            params: vec![Indirect(IndirectId("HL".to_string()))]
        };
        assert_eq!(operation_length(&op).unwrap(), 1);
    }
    
    #[test]
    fn test_section_split() {
        let stmts = vec![
            Statement::Section(0x0150),
            Statement::Operation(Op {
                mnemonic: "HALT".to_string(), params: vec![]
            }),
            Statement::Section(0x0300),
            Statement::Operation(Op {
                mnemonic: "HALT".to_string(), params: vec![]
            }),
        ];
        
        assert_eq!(
            split_sections(stmts),
            vec![
                Section {
                    start_address: 0x0150,
                    operations: vec![Op {mnemonic: "HALT".to_string(), params: vec![]}],
                    label_indexes: HashMap::<_,_>::new()
                },
                Section {
                    start_address: 0x0300,
                    operations: vec![Op {mnemonic: "HALT".to_string(), params: vec![]}],
                    label_indexes: HashMap::<_,_>::new()
                }
            ]
        );
    }
        
    #[test]
    fn test_address() {
        let mut labels = HashMap::<String, usize>::new();
        labels.insert("test".to_string(), 1);
        
        let section = Section {
            start_address: 0x0150,
            operations: vec![
                Op {mnemonic: "HALT".to_string(), params: vec![]},
                Op {mnemonic: "LD".to_string(), params: vec![
                    Indirect(IndirectImmediate(20)),
                    Identifier("SP".to_string())
                ]}
            ],
            label_indexes: labels
        };
        
        let mut label_addresses = HashMap::<String, u16>::new();
        label_addresses.insert("test".to_string(), 0x151);
        
        assert_eq!(
            assign_addresses(section),
            AddressedSection {
                start_address: 0x0150,
                length: 4,
                operations: vec![
                    Op {mnemonic: "HALT".to_string(), params: vec![]},
                    Op {mnemonic: "LD".to_string(), params: vec![
                        Indirect(IndirectImmediate(20)),
                        Identifier("SP".to_string())
                    ]}
                ],
                label_addresses: label_addresses
            }
        );
    }
    
    #[test]
    fn test_section_overlap() {
        let mut sections = [
            AddressedSection {
                start_address: 10,
                length: 10,
                operations: vec![],
                label_addresses: HashMap::new(),
            },
            AddressedSection {
                start_address: 0,
                length: 10,
                operations: vec![],
                label_addresses: HashMap::new(),
            }
        ];
        assert!(check_sections_dont_overlap(&mut sections).is_ok());
        
        let mut sections = [
            AddressedSection {
                start_address: 8,
                length: 10,
                operations: vec![],
                label_addresses: HashMap::new(),
            },
            AddressedSection {
                start_address: 0,
                length: 10,
                operations: vec![],
                label_addresses: HashMap::new(),
            }
        ];
        assert!(check_sections_dont_overlap(&mut sections).is_err());
    }
}
