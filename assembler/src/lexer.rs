// Based on the Tiny lexer by Markus Siemes
// https://github.com/msiemens/rust-tinyasm/blob/master/src/assembler/parser/lexer.rs
// Licensed under the MIT license

use std::fmt;
use std::rc::Rc;
use crate::util::fatal;


#[derive(PartialEq, Eq, Clone)]
pub enum Token<'a> {
    COLON,
    COMMA,

    SECTION,
    IDENT(&'a str),
    MNEMONIC(&'a str),
    UNSIGNED(u16),
    SIGNED(i8),

    LPAREN,
    RPAREN,

    EOF
}

impl<'a> fmt::Debug for Token<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Token::COLON      => write!(f, ":"),
            Token::COMMA      => write!(f, ","),

            Token::LPAREN     => write!(f, "("),
            Token::RPAREN     => write!(f, ")"),

            Token::SECTION => write!(f, "SECTION"),
            Token::MNEMONIC(ref mne)   => write!(f, "{:?}", mne),
            Token::IDENT(ref ident)   => write!(f, "{:?}", ident),
            Token::UNSIGNED(i)        => write!(f, "{}", i),
            Token::SIGNED(i)          => write!(f, "{}", i),

            Token::EOF         => write!(f, "EOF"),
        }
    }
}

impl<'a> fmt::Display for Token<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

pub type SharedString = Rc<String>;

#[derive(PartialEq, Eq, Clone)]
pub struct SourceLocation {
    pub filename: SharedString,
    pub lineno: usize
}

impl fmt::Debug for SourceLocation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}:{}", self.filename, self.lineno)
    }
}

impl fmt::Display for SourceLocation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}


// --- The Lexer ----------------------------------------------------------------
// We use a Lexer trait along with two implementations: FileLexer and Vec<Token>.
// The first one is used for processing a file on the hard drive, the second
// is used for testing purposes.

pub trait Lexer<'a> {
    fn get_source(&self) -> SourceLocation;
    fn next_token(&mut self) -> Token<'a>;
    fn tokenize(&mut self) -> Vec<Token<'a>>;
}


// --- The Lexer: FileLexer -----------------------------------------------------

pub struct FileLexer<'a> {
    source: &'a str,
    file: SharedString,
    len: usize,

    pos: usize,
    curr: Option<char>,

    lineno: usize
}

impl<'a> FileLexer<'a> {

    pub fn new(source: &'a str, file: &str) -> FileLexer<'a> {
        FileLexer {
            source: source,
            file: Rc::new(file.to_string()),
            len: source.len(),

            pos: 0,
            curr: source.chars().next(),

            lineno: 1
        }
    }


    // --- File Lexer: Helpers ---------------------------------------------------

    fn fatal(&self, msg: String) -> ! {
        fatal(msg, &self.get_source())
    }


    fn is_eof(&self) -> bool {
        self.curr.is_none()
    }


    // --- File Lexer: Character processing --------------------------------------

    fn bump(&mut self) {
        self.curr = self.nextch();
        self.pos += 1;

        // // debug!("Moved on to {:?}", self.curr)
    }

    // Panics if pos is not a char boundary
    fn source_char_at(&self, pos: usize) -> char {
        ::std::str::from_utf8(&self.source.as_bytes()[pos..]).unwrap()
            .chars().next().unwrap()
    }

    fn nextch(&self) -> Option<char> {
        let mut new_pos = self.pos + 1;

        // When encountering multi-byte UTF-8, we may stop in the middle
        // of it. Fast forward till we see the next actual char or EOF

        while !self.source.is_char_boundary(new_pos)
                && self.pos < self.len {
            new_pos += 1;
        }

        if new_pos < self.len {
            Some(self.source_char_at(new_pos))
        } else {
            None
        }
    }

    fn collect<F>(&mut self, cond: F) -> &'a str
            where F: Fn(&char) -> bool {
        let start = self.pos;

        // debug!("start collecting");

        while let Some(c) = self.curr {
            if cond(&c) {
                self.bump();
            } else {
                // debug!("colleting finished");
                break;
            }
        }

        let end = self.pos;

        &self.source[start..end]
    }

    fn eat_all<F>(&mut self, cond: F)
            where F: Fn(&char) -> bool {
        while let Some(c) = self.curr {
            if cond(&c) { self.bump(); }
            else { break; }
        }
    }

    // --- File Lexer: Tokenizers ------------------------------------------------

    fn tokenize_ident(&mut self) -> Token<'a> {
        // debug!("Tokenizing an ident");

        let ident = self.collect(|c| {
            c.is_alphabetic() || c.is_numeric() || *c == '_'
        });

        lazy_static! {
            static ref MNEMONICS: std::collections::HashSet<String> = [
                "NOP", "STOP", "JR", "LD", "ADD", "SUB", "AND", "OR", "RET",
                "LDH", "LDD", "LDI", "POP", "PUSH", "JP", "INC", "DI", "EI",
                "CALL", "DEC", "HALT", "RLCA", "RLA", "DAA", "SCF", "RST",
                "CP", "RET", "LDHL", "ADC", "SBC", "XOR", "RRCA", "RRA", "CPL",
                "CCF", "RLC", "RRC", "RL", "RR", "SLA", "SRA", "SRL", "SWAP",
                "BIT", "RES", "SET"
            ].iter().map(|s| s.to_string()).collect();
        }

        if MNEMONICS.contains(&ident.to_uppercase()) {
            Token::MNEMONIC(ident)
        } else if ident == "SECTION" {
            Token::SECTION
        } else {
            Token::IDENT(ident)
        }
    }

    fn tokenize_unsigned_dec(&mut self) -> Token<'a> {
        // debug!("Tokenizing an unsigned integer (decimal)");

        let unsigned_str = self.collect(|c| c.is_numeric());
        let unsigned     = match unsigned_str.parse() {
            Ok(i) => i,
            Err(_) => self.fatal(format!("invalid unsigned: {}", unsigned_str))
        };

        Token::UNSIGNED(unsigned)
    }

    fn tokenize_unsigned_hex(&mut self) -> Token<'a> {
        // debug!("Tokenizing an unsigned integer (hexadecimal)");

        let unsigned_str = self.collect(|c| c.is_ascii_hexdigit());
        let unsigned     = match u16::from_str_radix(unsigned_str, 16) {
            Ok(i) => i,
            Err(_) => self.fatal(format!("invalid unsigned: {}", unsigned_str))
        };

        Token::UNSIGNED(unsigned)
    }

    fn tokenize_signed(&mut self) -> Token<'a> {
        // debug!("Tokenizing a signed integer");

        let signed_str = self.collect(|c| c.is_numeric() || *c == '-' || *c == '+');
        let signed     = match signed_str.parse() {
            Ok(i) => i,
            Err(_) => self.fatal(format!("invalid signed: {}", signed_str))
        };

        Token::SIGNED(signed)
    }

    /// Read the next token and return it
    ///
    /// If `None` is returned, the current token is to be ignored and the
    /// lexer requests the reader to read the next token instead.
    fn read_token(&mut self) -> Option<Token<'a>> {
        let c = match self.curr {
            Some(c) => c,
            None    => return Some(Token::EOF)
        };

        let token = match c {
            ':' => { self.bump(); Token::COLON },
            ',' => { self.bump(); Token::COMMA },
            '(' => { self.bump(); Token::LPAREN },
            ')' => { self.bump(); Token::RPAREN },

            c if c.is_alphabetic()  => self.tokenize_ident(),
            c if c.is_numeric() => self.tokenize_unsigned_dec(),
            '$'                 => { self.bump(); self.tokenize_unsigned_hex() },
            '-' | '+' => self.tokenize_signed(),

            ';' => {
                self.eat_all(|c| *c != '\n');
                return None;
            },
            c if c.is_whitespace() => {
                if c == '\n' { self.lineno += 1; }

                self.bump();
                return None;
            },
            c => {
                self.fatal(format!("unknown token: {}", c))
                // UNKNOWN(format!("{}", c).into_string())
            }
        };

        Some(token)
    }
}

impl<'a> Lexer<'a> for FileLexer<'a> {
    fn get_source(&self) -> SourceLocation {
        SourceLocation {
            filename: self.file.clone(),
            lineno: self.lineno
        }
    }

    fn next_token(&mut self) -> Token<'a> {
        if self.is_eof() {
            Token::EOF
        } else {
            // Read the next token until it's not none
            loop {
                if let Some(token) = self.read_token() {
                    return token;
                }
            }
        }
    }

    #[allow(dead_code)]  // Used for tests
    fn tokenize(&mut self) -> Vec<Token<'a>> {
        let mut tokens = vec![];

        while !self.is_eof() {
            // debug!("Processing {:?}", self.curr);

            if let Some(t) = self.read_token() {
                tokens.push(t);
            }

            // debug!("So far: {:?}", tokens)
        }

        tokens
    }
}

pub fn dummy_source() -> SourceLocation {
    SourceLocation {
        filename: Rc::new("<input>".to_string()),
        lineno: 0
    }
}

// --- The Lexer: Vec<Token> ----------------------------------------------------

impl<'a> Lexer<'a> for Vec<Token<'a>> {
    fn get_source(&self) -> SourceLocation {
        dummy_source()
    }

    fn next_token(&mut self) -> Token<'a> {
        if self.len() >= 1 {
            self.remove(0)
        } else {
            Token::EOF
        }
    }

    fn tokenize(&mut self) -> Vec<Token<'a>> {
        self.iter().cloned().collect()
    }
}

#[cfg(test)]
mod test {
    use super::{Token, Lexer, FileLexer};
    use super::Token::*;

    fn tokenize(src: &'static str) -> Vec<Token> {
        FileLexer::new(src, "<test>").tokenize()
    }

    #[test]
    fn test_mnemonic() {
        assert_eq!(tokenize("HALT"),
                   vec![MNEMONIC("HALT")]);
    }
    
    #[test]
    fn test_section() {
        assert_eq!(tokenize("SECTION"),
                   vec![SECTION]);
    }

    #[test]
    fn test_ident() {
        assert_eq!(tokenize("abc"),
                   vec![IDENT("abc")]);
       assert_eq!(tokenize("ABC"),
                  vec![IDENT("ABC")]);
    }

    #[test]
    fn test_ident_with_underscore() {
        assert_eq!(tokenize("abc_efg"),
                   vec![IDENT("abc_efg")]);
    }

    #[test]
    fn test_dec_unsigned() {
        assert_eq!(tokenize("128"),
                   vec![UNSIGNED(128)]);
    }

    #[test]
    fn test_hex_unsigned() {
        assert_eq!(tokenize("$FF"),
                   vec![UNSIGNED(0xFF)]);
    }

    #[test]
    fn test_signed() {
        assert_eq!(tokenize("-5"),
                   vec![SIGNED(-5)]);
       assert_eq!(tokenize("+7"),
                  vec![SIGNED(7)]);
    }

    #[test]
    fn test_comment() {
        assert_eq!(tokenize("; asd"),
                   vec![]);
        assert_eq!(tokenize("; asd\nLD ;asd\nHALT"),
                   vec![MNEMONIC("LD"),
                        MNEMONIC("HALT")]);
    }

    #[test]
    fn test_whitespace() {
        assert_eq!(tokenize("\n\n\n\n     \n\t\n"),
                   vec![]);
        assert_eq!(tokenize("      LD        \n\n HALT"),
                   vec![MNEMONIC("LD"),
                        MNEMONIC("HALT")]);
    }

    #[test]
    fn test_line_counter() {
        let mut lx = FileLexer::new("LD\nLD", "<test>");
        lx.tokenize();
        assert_eq!(lx.lineno, 2);

        let mut lx = FileLexer::new("LD\r\nLD", "<test>");
        lx.tokenize();
        assert_eq!(lx.lineno, 2);
    }
}
