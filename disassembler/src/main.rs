extern crate argparse;
extern crate damaged_core;

use damaged_core::instruction::get_next_instruction;

use std::fs;
use std::io::Read;
use std::io::Result as ioResult;
use std::path::Path;

fn main() {
    let mut binary_path: String = "".into();
    let mut offset_str: String = "".into();
    let mut len: usize = 1;
    
    {
        let mut parser = argparse::ArgumentParser::new();

        parser.refer(&mut binary_path)
            .add_argument("binary", argparse::Store,
                          "Path to a Cartridge ROM")
            .required();
        parser.refer(&mut offset_str)
            .add_argument("offset", argparse::Store,
                          "Start of decoding (hexadecimal)")
            .required();
        parser.refer(&mut len)
            .add_argument("len", argparse::Store,
                          "Length of decoding");

        parser.parse_args_or_exit();
    }
    
    let mut offset = match usize::from_str_radix(&offset_str, 16) {
        Ok(v) => v,
        Err(_) => panic!("Invalid offset \"{}\"", offset_str),
    };
    let len = len;
    
    //Read binary data
    let binary_contents = match read_binary(binary_path) {
        Ok(contents) => contents,
        Err(error) => {
            panic!("Error reading bootrom: {:?}", error);
        }
    };
    
    
    let end = offset + len;
    while offset < end {
        let inst = get_next_instruction(&binary_contents, offset);
        println!("{:04X} : {}", offset, inst);
        offset += inst.bytes.len();
    }
}

fn read_binary<P: AsRef<Path>>(path: P) -> ioResult<Box<[u8]>> {
    let mut file = fs::File::open(path)?;
    let mut buffer = Vec::<u8>::new();

    file.read_to_end(&mut buffer)?;

    Ok(buffer.into_boxed_slice())
}